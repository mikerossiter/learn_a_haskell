badGreeting :: String
badGreeting = "Oh! Pfft it's you!"

niceGreeting :: String
niceGreeting = "Hello! It's so very nice to see you,"

greet :: String -> String
greet "Juan" = niceGreeting ++ " Juan!"
greet "Fernando" = niceGreeting ++ " Fernando!"
greet name = badGreeting ++ " " ++ name
